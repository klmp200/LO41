//
// Created by Antoine Bartuccio on 05/06/2018.
//

#ifndef LO41_BUILDING_H
#define LO41_BUILDING_H

#define ELEVATOR_NB 3
#define FLOORS 25

#include "../Objects.h"
#include "../List/List.h"
#include "../Elevator/Elevator.h"
#include "../Resident/Resident.h"
#include "../Visitor/Visitor.h"
#include "../Passenger/Passenger.h"

typedef struct o_Building {
	PRIVATE int floors;
	PRIVATE int waiting_floors[FLOORS];
	PRIVATE List * residents;
	PRIVATE List * visitors;
	PRIVATE Elevator ** elevators;
	PRIVATE pthread_mutex_t mutex_waiting_floors;
	PRIVATE pthread_mutex_t mutex_repair;
	PRIVATE pthread_mutex_t * mutex_cond_get_inside_elevator;
	PRIVATE pthread_mutex_t * mutex_cond_get_outside_elevator;
	PRIVATE pthread_mutex_t * mutex_func_get_inside_elevator;
	PRIVATE pthread_mutex_t * mutex_func_get_next_call;
	PRIVATE pthread_cond_t ** condition_floors;

	PRIVATE void (*parse_residents)(_THIS(Building), char * file);
	PRIVATE void (*parse_visitors)(_THIS(Building), char * file);
	PRIVATE int (*get_inside_elevator)(_THIS(Building), int current_floor, Passenger * passenger);

	PUBLIC int (*use_call_box)(_THIS(Building), char * resident_name);
	PUBLIC void (*signal_elevator_at_floor)(_THIS(Building), int floor);
	PUBLIC int (*get_next_call)(_THIS(Building), int elevator_floor);
	PUBLIC int* (*get_waiting_floors)(_THIS(Building));

	SYNCHRONIZE PUBLIC void (*ask_elevator_reparation)(_THIS(Building), Elevator* elevator);

	SYNCHRONIZE PUBLIC void (*go_to_floor)(_THIS(Building), int origin, int destination, Passenger * passenger);

	DESTRUCTOR(Building);
} Building;

FRIENDLY(residents, SharedData)
FRIENDLY(visitors, SharedData)
FRIENDLY(elevators, SharedData)

Building *_init_Building(char * residents_file, char * visitors_file);

#endif //LO41_BUILDING_H
