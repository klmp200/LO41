//
// Created by Antoine Bartuccio on 22/05/2018.
//

#include "Objects.h"

void *malloc_or_die(size_t size){
	void *ptr = malloc(size);
	if (ptr == NULL){
		CRASH("Error allocating object");
	}
	return ptr;
}

void agent_exit(int error_code){
	printf("Agent receiving code %d, exiting\n", error_code);
	pthread_exit(NULL);
}
