#!/bin/perl

# --- Documentation ---
# Syntax : ./generate.pl agents_number [floor_number]
# agents_number : can specify a number ranging from 2 to 500 ( upgradable by upgrading database ). number of visitors and residents are evenly splitted.
# floor_number : number of floor in building ( not counting floor 0 ), ranging from 1 to whatever number is too big for perl. By default, number of floor is 25.
# the scripts will generate two files : visitors.txt and residents.txt
# visitors.txt syntax : each line is : visitor_name;resident_name\n
# residents.txt syntax : each line is : resident_name;resident_floor;destination_floor\n
# resident_floor is ranging from 1 to floor_number ( implying residents cant live at floor 0 ), while destination_floor is ranging from 0 to floor_number

# written by Arthur Amalvy
# last modification : 18/06/18

use strict;
use warnings;
use List::Util qw/shuffle/;

die "Usage : generate.pl agents_number [floor_number]\nagents number range from 2 to 500\nfloor number range from 1 to +inf\n" if @ARGV == 0;
my $total_nb = $ARGV[0];
die "Error : stupid amount of agents ($total_nb)\n" if $total_nb < 2;
my $visitors_nb = int($total_nb / 2);
my $residents_nb = $total_nb - $visitors_nb;
my $floor_number = @ARGV > 1 ? $ARGV[1] : 25;
die "Error : stupid floor number ($floor_number)\n" if $floor_number < 1;

my $visitors_output_file_name = "visitors.txt";
my $residents_output_file_name = "residents.txt";
my $visitors_db_filename = "visitors.db";
my $residents_db_filename = "residents.db";
my @visitors = ();
my @residents = ();


open(my $visitors_db_file, $visitors_db_filename) or die "Error : no visitors database found \n";

open(my $residents_db_file, $residents_db_filename) or die "Error : no residents database found \n";

while(my $line = <$visitors_db_file>){
    chomp($line);
    push(@visitors, $line);
}
while(my $line = <$residents_db_file>){
    chomp($line);
    push(@residents, $line) if(@residents < $residents_nb);
}

die "Database is not big enough for $visitors_nb visitors\n" if @visitors < $visitors_nb;
die "Database is not big enough for $residents_nb residents" if @residents < $residents_nb;

@visitors = shuffle(@visitors);
@residents = shuffle(@residents);

open(my $visitors_output_file, '>', $visitors_output_file_name) or die "Error : couldn't create visitors output file.";
open(my $residents_output_file, '>', $residents_output_file_name) or die "Error : couldn't create residents output file.";


for(my $i = 0; $i < $visitors_nb; $i += 1){
    print $visitors_output_file $visitors[$i] . ";" .
	$residents[rand $residents_nb] . "\n";
}

for(my $i = 0; $i < $residents_nb; $i += 1){
    print $residents_output_file $residents[$i] . ";" . 
	(int(rand($floor_number)) + 1) . ";" .
	(int(rand($floor_number + 1))) . "\n";
}

close $visitors_db_file;
close $residents_db_file;
close $visitors_output_file;
close $residents_output_file;

