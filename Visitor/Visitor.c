//
// Created by Antoine Bartuccio on 06/06/2018.
//

#include "Visitor.h"
#include "../SharedData/SharedData.h"
#include <string.h>

GETTER(Visitor, char*, name);
GETTER(Visitor, int, destination);
GETTER(Visitor, int, id);
SETTER(Visitor, int, thread_number);

void * runnable_Visitor(void * void_this){
	Visitor *this = (Visitor*) void_this;
	SharedData * data = GET_INSTANCE(SharedData);
	Passenger * passenger = NEW(Passenger, (void*) this, VISITOR);

	AGENT_OPTIONS;
	this->passenger = (void*) passenger;

	passenger->visitor = this;

	printf("Visiteur %s : Je souhaite rendre visite a %s\n", this->name, this->contact_name);
	printf("Visiteur %s : J'apelle à l'interphone\nVisiteur %s : J'apprends que %s habite à l'étage %d\n", this->name, this->name, this->contact_name, (this->destination = data->use_call_box(data, this->contact_name)));
	if (this->destination == this->position)
		printf("Visiteur %s : pas besoin de prendre l'ascenseur pour cet étage, je vais y aller à pied\n", this->name);
	else if (this->destination == -1)
		printf("Visiteur %s : Je me suis trompé de bâtiment, %s n'habite pas ici.\n", this->name, this->contact_name);
	else
		data->main_building->go_to_floor(data->main_building, this->position, this->destination, passenger);

	data->decrement_active_passengers(data);
	data->unregister_thread(data, this->thread_number);
	return NULL;
}

void _free__Visitor(THIS(Visitor)){
	if (this->name != NULL)
		free(this->name);
	if (this->contact_name != NULL)
		free(this->contact_name);
	if (this->passenger != NULL)
		free(this->passenger);
	free(this);
}

Visitor *_init_Visitor(int id, char* name, char * contact_name){
	Visitor * new_visitor = malloc_or_die(sizeof(Visitor));
	new_visitor->name = strdup(name);
	new_visitor->id = id;
	new_visitor->position = 0;
	new_visitor->destination = -1;
	new_visitor->passenger = NULL;
	new_visitor->thread_number = -1;

	if (contact_name != NULL)
		new_visitor->contact_name = strdup(contact_name);
	else
		new_visitor->contact_name = NULL;

	LINK_ALL(Visitor, new_visitor,
			 get_name,
			 get_destination,
			 get_id,
			 runnable,
			 set_thread_number
	);

	return new_visitor;
}
