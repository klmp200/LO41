//
// Created by Antoine Bartuccio on 06/06/2018.
//

#ifndef LO41_VISITOR_H
#define LO41_VISITOR_H

#include "../Objects.h"

typedef struct o_Visitor {
	PRIVATE int id;
	PRIVATE int thread_number;
	PRIVATE char * name;
	PRIVATE char * contact_name;
	PRIVATE int position;
	PRIVATE int destination;
	PRIVATE void * passenger;

	PUBLIC void * (*runnable)(void* void_this);
	PUBLIC char * (*get_name)(_THIS(Visitor));
	PUBLIC int (*get_id)(_THIS(Visitor));
	PUBLIC int (*get_destination)(_THIS(Visitor));
	PUBLIC void (*set_thread_number)(_THIS(Visitor), int data);

	DESTRUCTOR(Visitor);
} Visitor;

FRIENDLY(name, Building)

Visitor *_init_Visitor(int id, char* name, char* contact_name);

#endif //LO41_VISITOR_H
