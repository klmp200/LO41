Rapport projet LO41 Bartuccio Antoine et Amalvy Arthur
====

# Interprétation du sujet

Ce sujet de LO41 étant volontairement vague pour permettre de nombreuses implémentations différentes, et la contrainte vis à vis du temps étant importante, il était impossible d'ajouter toutes les fonctionnalitées proposées. Il a donc fallu opérer un choix pragmatique afin de rendre un travail le plus complet possible tout en s'approchant un maximum de la vision originelle du sujet.

Dans un premier temps, nous avons répertorié les contraintes auxquelles nous ne pouvions nous soustraire. C'est donc bien évidemment que nous avons conservé le bâtiment de 25 étages ainsi que les 3 ascenseurs et la borne interactive au pied de ce dernier. C'est à ce moment là que se sont posées les questions les plus importantes et pouvant potentiellement modifier complètement le résultat du projet : est-ce seulement la borne qui permet d'appeler les ascenseurs ? Auquel cas, puisqu'elle est au pied de l'immeuble personne ne peut redescendre autrement qu'en prenant les escaliers. Devons nous utiliser un bouton par ascenseur ou un système d'appel centralisé ? Si on ajoute un bouton pour les ascenseurs à chaque étage, à quoi sert donc la borne au final ? Est-elle uniquement dédiée aux visiteurs ou est-elle utilisable par les résidents ? Est-elle vraiment pertinente ? La rendre indépendante est-il vraiment un choix intéressant ? Allons nous empêcher certains ascenseurs d'accéder à certains étages ou faire en sorte qu'ils aient tous accès à l'entièreté de l'immeuble ? Comment faire pour le dépannage ? Comment peut-on déterminer qu'un technicien est plus à même d'intervenir ? Doit-on avoir plusieurs types de pannes nécessitant différents outils ? Combien tout ceci va-t-il coûter à la copropriété ?

Nous avons donc commencé par trancher sur l'utilisation de la borne. Cette borne aura une utilité assez limitée et sera cantonée à la recherche de l'étage d'un résident par un visiteur. Globalement, elle sera simulée par une fonction renvoyant l'étage d'un résident à partir de son nom. L'appel des ascenseur se fera en interrogeant directement le bâtiment qui se chargera tout seul d'indiquer un ascenseur à partir simplement de l'étage de départ et de l'étage d'arrivée souhaité. L'ordonancement des ascenseurs se fera donc directement depuis le bâtiment.

Nous en arrivons donc logiquement à une distinction visiteurs et résidents. Les visiteurs démarrent à leur étage d'habitation et se déplacent ou non selon leur envie. Les visiteurs, quand à eux, souhaitent rejoindre l'étage d'un résident dont ils connaissent uniquement le nom, ils demandent donc à la borne où celui-ci réside, et tentent d'y accéder en utilisant les ascenceurs.

Enfin, pour les réparations, il a été décidé, afin d'éviter de peser trop fortement sur le budget de la copropriété, d'engager un seul réparateur prêt à répondre à tous les cas pratique et toutes les pannes. Il sera appelé par les ascenseurs qui détecteront automatiquement les pannes et attendront leur réparation selon la disponibilité de cette ressource critique.


# Mise en place de l'architecture

## Le choix des moniteurs et des threads

Dans le cadre de l'UV LO41, nous avons eu l'occasion d'expérimenter et de tester différentes méthodes de parallélisation via l'API du système Linux et UNIX. Nous avons donc dû effectuer un choix crucial : utiliser des processus indépendants ou un seul processus avec plusieurs threads.

Notre choix s'est porté sur l'utilisation de threads et de moniteurs. En effet, ils sont bien plus simples d'usage, puisque toute la mémoire du programme est partagée, permettant une communication efficiente et simple entre les différentes sections indépendantes de celui-ci. De plus, en cas d'extinction non contrôlée du programme (particulièrement pratique en phase de tests), il est simple d'opérer vis-à-vis de l'extinction des threads : On évite ainsi tout processus zombie, et donc l'atteinte de la limite maximum de processus système. 

Mais surtout, ce qui a le plus fait pencher la balance en faveur des moniteurs est le fait que cette technologie est présente dans des languages de plus haut niveau tel que le java. En effet, on retrouve ce genre de mécanisme directement intégré au language via le mot clef synchronize. C'est ce type de comportement que nous avons souhaité imiter.

## Une architecture orientée objet

En observant le language java, nous avons remarqué qu'une architecture orientée objet, avec son encapsulation, était particulièrement adaptée à la parallélisation, et notamment dans le cadre d'utilisation des moniteurs. C'est donc sur ce concept solide et éprouvé que nous avons construit notre projet.

Petit problème, nous sommes contraint, de par le sujet, à utiliser le langage C. Ce langage très populaire, inventé en 1972 par Dennis Ritchie, n'est pas pensé pour ce genre d'approche. Il a donc fallu mettre en place bon nombre de stratégies pour rendre cohérente et agréable une approche de programmation non prévue par notre outil. Nous avons poussé le langage dans ses retranchements grâce à de nombreuses macros de manière à modifier la syntaxe selon nos besoins.

Le temps consacré à la mise en place de cette structure est loin d'avoir été perdu et nous a permis de gagner en consistance et en clarté dans notre code. Les fuites de mémoires sont très rares et faciles à régler, le lancement des threads est très simple et ils peuvent être stoppés à tout moment grâce à l'utilisation d'un singleton persistant contenant les différents objets systèmes. Le partage de la mémoire est très simple grâce à une utilisation de getter et setter encapsulant les mutexs. Les interblocages sont quasiment impossibles à réaliser de cette manière.

Voici, pour illustrer, le très concis thread principal de notre programme qui permet d'apprécier à sa juste valeur les modifications apportées à la syntaxe et à l'agencement des structures pour les faire ressembler à des objets :

```c
int main(int argc, char* argv[]) {
	SharedData * shared_data = GET_INSTANCE(SharedData);

	signal(SIGINT, clean_exit);
	if(argc == 3){
		shared_data->set_main_building(shared_data, NEW(Building, argv[1], argv[2]));
	} else if (argc == 1){
		shared_data->set_main_building(shared_data, NEW(Building, "../residents.txt", "../visitors.txt"));
	} else{
		CRASH("Arguments invalides\nUsage : ./LO41 [residents_file visitors_file]\n");
	}

	shared_data->start_all_threads(shared_data);
	shared_data->wait_all_threads(shared_data);
	DELETE(shared_data);

	return 0;
}
```

## L'introduction des agents

De l'objet à l'agent il n'y a qu'un pas, l'indépendance. Enfin, pas vraiment, mais presque. Nous avons eu l'occasion lors de notre cursus de travailler sur un langage orienté agent : le SARL. Même si celui-ci reste perfectible, il a su nous inspirer lors de la conception de ce projet. Même si nous n'avons pas le temps d'implémenter de la communication entre agents dans des contextes séparés le tout en architecture holonique, nous avons repris l'idée de l'agent et l'avons adaptée à notre architecture et notre projet.

Globalement, il existe 4 types d'agents dans ce projet : les ascenseurs, les visiteurs, les résidents et le casseur d'ascenseur (pour casser les ascenseurs de temps en temps). Ils sont chacun lancés dans leur propre thread et tentent d'atteindre leur objectif indépendamment tout en interagissant avec les autres. Ils réagissent également lorsqu'ils reçoivent un signal d'apoptose les invitant à mettre fin à leur existence dans le but de libérer les ressources, dans le cadre d'une fin prévue ou non du programme.

Pour y parvenir, nous avons attaché à chacun de ces trois objets une méthode `runnable`, qui prend en paramètre une référence vers l'objet lui même et qui configure le thread de manière à répondre de manière normalisée aux signaux. En effet, chaque agent se doit de répondre correctement aux signaux d'apoptose (attachés à SIGUSR1) et d'ignorer les signaux d'arrêt (SIGINT) pour ne pas qu'ils l'interceptent à la place du thread principal puisque la réception de signal dans un environnement multithread n'est pas predictive.

Attention en utilisant l'API pthread, envoyer un signal (avec `pthread_kill`) vers un thread terminé a un comportement non défini d'après la spécification du standard. On observe ainsi des comportements très variés selon l'OS. Pour pallier à cela, nous avons mis en place un système de déréférencement des threads terminés.

## Le choix des scénarios prétirés

Dans ce projet, nous avons fait le choix de ne pas effectuer de génération aléatoire de clients. En effet, le programme opère vis-à-vis de scénarios pré-tirés injectables directement à l'exécution du programme. Cela permet notamment de facilement reproduire en phase de développement les différents situations pratiques non souhaitées par le développeur dans une optique d'amélioration de la stabilité du programme.. Ces scenarios se présentent sous la forme de fichiers textes au format csv où les différentes données sont séparées par des points virgule.

# Architecture

## Diagramme des classes

Puisque nous respectons aussi rigoureusement que possible une architecture objet, nous sommes en mesure de vous fournir un diagramme de classe du projet.

Nous avons tout d'abord commencé par faire une liste chaînée afin de tester notre architecture objet.
![](https://i.imgur.com/rcbNfz2.png)

Voici enfin ce que donne l'architecture du projet.
![](https://i.imgur.com/0QwVT5k.png)

On observe en particulier la singularité de l'objet SharedData, vis-à-vis en tout cas de ses pairs. En effet, celui-ci opère en tant que singleton et permet notamment de référencer et déréférencer tous les threads, et ce quelque soit le contexte dans lequel opère le programme.


## Réseau de Pétri

Même si notre architecture initiale limite déjà fortmenet les possibilités d'interblocages, il était judicieux de modéliser de manière abstraite le fonctionnement théorique et basique de l'attente d'un utilisateur à un étage à l'aide d'un réseau de Pétri. La priorité est ici simulée par un nombre d'étapes plus ou moins faible pour chaque ascenseur. Il faut cependant ne pas oublier que les ascenseurs devant être intelligents et autonomes, il est impossible avec un outil tel que le réseau de pétri de modéliser fidèlement leur comportement. Ce réseau ne donne donc qu'une vision simplifiée du principe et l'implémentation s'en éloigne parfois.

![](https://i.imgur.com/nbTpyfB.png)

# Guide d'utilisation

## Phase de compilation

Le logiciel utilise la technologie cmake pour gérer le projet. Voici donc les étapes nécessaires à sa compilation.

```shell
mkdir out
cd out
cmake ..
make
```

## Exécution

Ce programme peut s'exécuter avec ou sans arguments. Sans arguments, le logiciel ouvre automatiquement les fichiers ../residents.txt et ../visitors.txt. Avec arguments, il est possible de choisir de lancer des scénarios personnalisés.

```shell
./LO41 [fichier_residents fichier_visiteurs]
./LO41 # -> correspond à ./LO41 ../residents.txt ../visitors.txt 
```