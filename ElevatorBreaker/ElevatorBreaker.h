//
// Created by Antoine Bartuccio on 22/06/2018.
//

#ifndef LO41_ELEVATORBREAKER_H
#define LO41_ELEVATORBREAKER_H

#define MAX_BREAKS 30

#include "../Objects.h"

typedef struct o_ElevatorBreaker {
	PRIVATE int thread_number;
	PRIVATE int breaks;

	PUBLIC void * (*runnable)(void * void_this);
	PUBLIC void (*set_thread_number)(_THIS(ElevatorBreaker), int data);

	DESTRUCTOR(ElevatorBreaker);

} ElevatorBreaker;

ElevatorBreaker *_init_ElevatorBreaker();

#endif //LO41_ELEVATORBREAKER_H
