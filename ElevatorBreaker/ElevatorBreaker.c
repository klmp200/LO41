//
// Created by Antoine Bartuccio on 22/06/2018.
//

#include <unistd.h>

#include "ElevatorBreaker.h"
#include "../SharedData/SharedData.h"

SETTER(ElevatorBreaker, int, thread_number);

void _free__ElevatorBreaker(THIS(ElevatorBreaker)){
	free(this);
}

void * runnable_ElevatorBreaker(void * void_this){
	ElevatorBreaker *this = (ElevatorBreaker*) void_this;
	Elevator *selected_elevator = NULL;
	SharedData *data = GET_INSTANCE(SharedData);

	AGENT_OPTIONS

		while (this->breaks < MAX_BREAKS && data->is_active_passengers_left(data)){
		usleep(900000);
		// One chance out of two to break something
		if ((rand() % 101) > 50) {
			selected_elevator = data->get_main_building(data)->elevators[rand() % ELEVATOR_NB];
			selected_elevator->damage(selected_elevator);
			printf("ALERTE : L'ascenseur %s est endommagé\n", selected_elevator->name);
			this->breaks++;
		}
	}

	data->unregister_thread(data, this->thread_number);
	return NULL;
}

ElevatorBreaker *_init_ElevatorBreaker(){
	ElevatorBreaker *new_elevator_breaker = malloc_or_die(sizeof(ElevatorBreaker));
	new_elevator_breaker->thread_number = -1;
	new_elevator_breaker->breaks = 0;

	LINK_ALL(ElevatorBreaker, new_elevator_breaker,
			 set_thread_number,
			 runnable
	)

	return new_elevator_breaker;
}
