//
// Created by Antoine Bartuccio on 05/06/2018.
//

#ifndef LO41_ELEVATOR_H
#define LO41_ELEVATOR_H

#include <pthread.h>
#include "../Objects.h"
#include "../List/List.h"
#include "../Passenger/Passenger.h"

//#define MAX_ELEVATOR_CAPACITY 10
#define MAX_ELEVATOR_CAPACITY 12

typedef enum {RUNNING, WAITING, SLEEPING, BROKEN} ELEVATOR_STATE;

typedef struct o_Elevator {
	PRIVATE ELEVATOR_STATE state;
	PRIVATE List * passengers;
	PRIVATE char * name;
	PRIVATE int floor;
	PRIVATE int target_floor;
	PRIVATE int thread_number;
	PRIVATE pthread_mutex_t mutex_passengers;
	PRIVATE pthread_mutex_t mutex_state;
	PRIVATE pthread_mutex_t mutex_floor;

	PUBLIC void * (*runnable)(void * void_this);
	PUBLIC void (*set_thread_number)(_THIS(Elevator), int data);

	SYNCHRONIZE PRIVATE void (*set_state)(_THIS(Elevator), ELEVATOR_STATE var);
	SYNCHRONIZE PRIVATE void (*set_floor)(_THIS(Elevator), int var);
	SYNCHRONIZE PRIVATE int (*get_next_passenger_stop)(_THIS(Elevator));

	SYNCHRONIZE PUBLIC void (*repair)(_THIS(Elevator));
	SYNCHRONIZE PUBLIC void (*damage)(_THIS(Elevator));
	SYNCHRONIZE PUBLIC int (*get_number_of_passengers)(_THIS(Elevator));
	SYNCHRONIZE PUBLIC void (*remove_passenger) (_THIS(Elevator), Passenger * passenger);
	SYNCHRONIZE PUBLIC void (*add_passenger)(_THIS(Elevator), Passenger * passenger);
	SYNCHRONIZE PUBLIC ELEVATOR_STATE (*get_state)(_THIS(Elevator));
	SYNCHRONIZE PUBLIC int (*get_floor)(_THIS(Elevator));
	SYNCHRONIZE PUBLIC int (*can_get_inside)(_THIS(Elevator), int floor);
	DESTRUCTOR(Elevator);

} Elevator;

FRIENDLY(state, Building)

Elevator *_init_Elevator(char* name);

#endif //LO41_ELEVATOR_H
