//
// Created by Antoine Bartuccio on 06/06/2018.
//

#include <string.h>
#include <pthread.h>
#include "Resident.h"
#include "../SharedData/SharedData.h"

GETTER(Resident, char *, name);
GETTER(Resident, int, destination);
GETTER(Resident, int, id);
GETTER(Resident, int, apartment_floor);
SETTER(Resident, int, thread_number);

void * runnable_Resident(void * void_this){
	Resident * this = (Resident*) void_this;
	SharedData* data = GET_INSTANCE(SharedData);
	Passenger * passenger = NEW(Passenger, (void*) this, RESIDENT);

	AGENT_OPTIONS;

	this->passenger = passenger;
	passenger->resident = this;
	passenger->type = RESIDENT;

	printf("Résident %s : Je suis a l'etage %d, je vais en direction de l'etage %d\n",
		this->name, this->apartment_floor, this->destination);
	if(this->position == this->destination)
		printf("Résident %s : Je reste chez moi\n", this->name);
	else
		data->main_building->go_to_floor(data->main_building, this->position, this->destination, passenger);
	data->decrement_active_passengers(data);
	data->unregister_thread(data, this->thread_number);
	return NULL;
}

void _free__Resident(THIS(Resident)){
	if (this->name != NULL)
		free(this->name);
	if(this->passenger != NULL)
		free(this->passenger);
	free(this);
}

Resident *_init_Resident(int id, char* name, int apartment_floor, int destination){
	/* If the destination is the same as the apartment_floor or negative, the RESIDENT will not move */
	Resident * new_resident = malloc_or_die(sizeof(Resident));
	new_resident->name = strdup(name);
	new_resident->id = id;
	new_resident->apartment_floor = apartment_floor;
	new_resident->position = new_resident->apartment_floor;
	new_resident->destination = destination;
	new_resident->passenger = NULL;
	new_resident->thread_number = -1;

	LINK_ALL(Resident, new_resident,
			 get_name,
			 get_destination,
			 get_id,
			 runnable,
			 get_apartment_floor,
			 set_thread_number
	)

	return new_resident;
}
