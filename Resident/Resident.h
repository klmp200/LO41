//
// Created by Antoine Bartuccio on 06/06/2018.
//

#ifndef LO41_RESIDENT_H
#define LO41_RESIDENT_H

#include "../Objects.h"

typedef struct o_Resident {
	PRIVATE int id;
	PRIVATE int thread_number;
	PRIVATE int apartment_floor;
	PRIVATE int destination;
	PRIVATE int position;
	PRIVATE char* name;
	PRIVATE void* passenger;

	PUBLIC void * (*runnable)(void * void_this);
	PUBLIC char * (*get_name)(_THIS(Resident));
	PUBLIC int (*get_id)(_THIS(Resident));
	PUBLIC int (*get_apartment_floor)(_THIS(Resident));
	PUBLIC int (*get_destination)(_THIS(Resident));
	PUBLIC void (*set_thread_number)(_THIS(Resident), int data);

	DESTRUCTOR(Resident);
} Resident;

FRIENDLY(name, Building)

Resident *_init_Resident(int id, char * name, int apartment_floor, int destination);

#endif //LO41_RESIDENT_H
