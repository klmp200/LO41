//
// Created by Antoine Bartuccio on 20/06/2018.
//

#include "Passenger.h"

int compare_Passenger(void * passenger1, void * passenger2){
	return (strcmp(((Passenger*) passenger1)->get_name((Passenger*) passenger1),
		       ((Passenger*) passenger2)->get_name((Passenger*) passenger2)) == 0);
}

int get_destination_Passenger(THIS(Passenger)){
	if (this->type == RESIDENT)
		return this->resident->get_destination(this->resident);
	if (this->type == VISITOR)
		return this->visitor->get_destination(this->visitor);
	return -1;
}

int get_id_Passenger(THIS(Passenger)){
	if (this->type == RESIDENT)
		return this->resident->get_id(this->resident);
	if (this->type == VISITOR)
		return this->visitor->get_id(this->visitor);
	return -1;
}

char *get_name_Passenger(THIS(Passenger)){
	if (this->type == RESIDENT)
		return this->resident->get_name(this->resident);
	if (this->type == VISITOR)
		return this->visitor->get_name(this->visitor);
	return NULL;
}

void *runnable_Passenger(void * void_this){
	Passenger *this = (Passenger*) void_this;
	if (this->type == RESIDENT)
		this->resident->runnable((void*)this->resident);
	if (this->type == VISITOR)
		this->visitor->runnable((void*)this->visitor);
	return NULL;
}

void set_thread_number_Passenger(THIS(Passenger), int thread_number){
	if (this->type == RESIDENT)
		this->resident->set_thread_number(this->resident, thread_number);
	if (this->type == VISITOR)
		this->visitor->set_thread_number(this->visitor, thread_number);
}

void _free__Passenger(THIS(Passenger)){
	free(this);
}

Passenger *_init_Passenger(void* passenger_data, PASSENGER_TYPE type){
	Passenger * new_passenger = malloc_or_die(sizeof(Passenger));

	new_passenger->resident = NULL;
	new_passenger->visitor = NULL;
	new_passenger->type = type;
	if (type == RESIDENT)
		new_passenger->resident = (Resident*) passenger_data;

	if (type == VISITOR)
		new_passenger->visitor = (Visitor*) passenger_data;

	//new_passenger->compare = compare_Passenger;
	LINK_ALL(Passenger, new_passenger,
		 get_id,
		 get_name,
		 get_destination,
		 compare,
		 runnable
	)

	return new_passenger;
}
