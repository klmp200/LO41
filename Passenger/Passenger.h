//
// Created by Antoine Bartuccio on 18/06/2018.
//

#ifndef LO41_PASSENGER_H
#define LO41_PASSENGER_H

#include "../Resident/Resident.h"
#include "../Visitor/Visitor.h"
#include <string.h>

typedef enum {RESIDENT, VISITOR} PASSENGER_TYPE;

typedef struct o_Passenger {
	PUBLIC PASSENGER_TYPE type;
	PUBLIC union {
		Resident * resident;
		Visitor * visitor;
	};

	PUBLIC char * (*get_name)(_THIS(Passenger));
	PUBLIC int (*get_id)(_THIS(Passenger));
	PUBLIC int (*get_destination)(_THIS(Passenger));
	PUBLIC void * (*runnable)(void* void_this);
	PUBLIC int (*compare)(void * passenger1, void * passenger2);//yeah I know, but i needed int (*) (void*, void*)
	PUBLIC void (*set_thread_number)(_THIS(Passenger), int thread_number);

	DESTRUCTOR(Passenger);
} Passenger;

Passenger *_init_Passenger(void* passenger_data, PASSENGER_TYPE type);

#endif //LO41_PASSENGER_H
