cmake_minimum_required(VERSION 3.9)
project(LO41 C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Werror -Wextra -Wshadow -fpic")

if(CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread")
endif()

add_executable(LO41 main.c List/List.h List/List.c Objects.h List/Element.c List/Element.h Objects.c Building/Building.c Building/Building.h Elevator/Elevator.c Elevator/Elevator.h SharedData/SharedData.c SharedData/SharedData.h Visitor/Visitor.c Visitor/Visitor.h Resident/Resident.c Resident/Resident.h Passenger/Passenger.h Passenger/Passenger.c ElevatorBreaker/ElevatorBreaker.c ElevatorBreaker/ElevatorBreaker.h)