//
// Created by Antoine Bartuccio on 06/06/2018.
//

#ifndef LO41_SHAREDDATA_H
#define LO41_SHAREDDATA_H

#include <pthread.h>
#include "../Objects.h"
#include "../Building/Building.h"
#include "../ElevatorBreaker/ElevatorBreaker.h"

typedef struct o_SharedData {
	PRIVATE int threads_nb;
	PRIVATE int active_passengers;
	PRIVATE ElevatorBreaker * elevator_breaker;
	PRIVATE pthread_t ** threads;
	PRIVATE Building * main_building;
	PRIVATE pthread_mutex_t mutex_active_passengers;
	PRIVATE pthread_mutex_t mutex_threads;

	PRIVATE void (*start_thread)(_THIS(SharedData), void * (*thread)(void *), void * data, int thread_number);

	PUBLIC void (*wait_all_threads)(_THIS(SharedData));
	PUBLIC void (*start_all_threads)(_THIS(SharedData));
	PUBLIC void (*set_main_building)(_THIS(SharedData), Building * building);
	PUBLIC Building * (*get_main_building)(_THIS(SharedData));
	PUBLIC int (*call_elevator)(_THIS(SharedData), int starting_floor, int destination_floor);
	PUBLIC int (*use_call_box)(_THIS(SharedData), char * resident_name);
	SYNCHRONIZE PUBLIC void (*decrement_active_passengers)(_THIS(SharedData));
	SYNCHRONIZE PUBLIC int (*is_active_passengers_left)(_THIS(SharedData));
	SYNCHRONIZE PUBLIC void (*unregister_thread)(_THIS(SharedData), int thread_number);

	DESTRUCTOR(SharedData);
} SharedData;

SharedData *_get_instance_SharedData();

#endif //LO41_SHAREDDATA_H
