//
// Created by Antoine Bartuccio on 22/05/2018.
//

#ifndef LO41_ELEMENT_H
#define LO41_ELEMENT_H

typedef struct o_List List;

#include "List.h"

struct o_Element {
	PRIVATE List *list;
	PRIVATE void *data;
	PRIVATE struct o_Element *next;
	PRIVATE struct o_Element *previous;
	PRIVATE void (*data_free)(void *);

	PUBLIC struct o_Element *(*get_next)(_THIS(Element));
	PUBLIC struct o_Element *(*get_previous)(_THIS(Element));
	PUBLIC void *(*get_data)(_THIS(Element));

	PUBLIC void (*set_previous)(_THIS(Element), struct o_Element *previous);
	PUBLIC void (*set_next)(_THIS(Element), struct o_Element *next);

	DESTRUCTOR(Element);
};

FRIENDLY(list, List)
FRIENDLY(data, List)
FRIENDLY(next, List)
FRIENDLY(previous, List)
FRIENDLY(data_free, List)

Element *_init_Element(void *data, size_t size, List *list);

#endif //LO41_ELEMENT_H
