//
// Created by Antoine Bartuccio on 22/05/2018.
//

#include <string.h>
#include "List.h"

GETTER(Element, Element*, next)
GETTER(Element, Element*, previous)
GETTER(Element, void*, data)

SETTER(Element, Element*, previous)
SETTER(Element, Element*, next)

void _free__Element(THIS(Element)){
	List * list = this->list;
	if (list != NULL) {
		if (list->head == this && list->tail == this) {
			list->head = NULL;
			list->tail = NULL;
		} else if (list->head == this) {
			list->head = this->next;
			this->previous = NULL;
		} else if (list->tail == this) {
			list->tail = this->previous;
			this->previous->next = NULL;
		} else {
			this->next->previous = this->previous;
			this->previous->next = this->next;
		}
		list->size --;
	}
	if (this->data != NULL)
		this->data_free(this->data);
	free(this);
}

Element *_init_Element(void *data, size_t size, List *list){
	Element *el = (Element*) malloc_or_die(sizeof(Element));
	void *new_data = NULL;

	if (data != NULL) {
		new_data = malloc_or_die(size);
		memcpy(new_data, data, size);
	}

	el->list = list;
	el->data = new_data;
	el->previous = NULL;
	el->next = NULL;
	el->data_free = free;

	if (el->list != NULL)
		el->list->size ++;

	LINK_ALL(Element, el,
			 get_next,
			 get_previous,
			 get_data,
			 set_next,
			 set_previous
	)

	return el;
}
