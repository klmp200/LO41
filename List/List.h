//
// Created by Antoine Bartuccio on 22/05/2018.
//

#ifndef LO41_LIST_H
#define LO41_LIST_H

#include "../Objects.h"

typedef struct o_Element Element;

#include "Element.h"

#define OUTSIDE_BOUNDS CRASH("You are outside of the list\n")

struct o_List {
	PRIVATE Element *head;
	PRIVATE Element *tail;
	PRIVATE int size;
	PRIVATE void (*custom_free)(void *);

	PUBLIC Element *(*get_head)(_THIS(List));
	PUBLIC Element *(*get_tail)(_THIS(List));

	PUBLIC Element *(*get_element)(_THIS(List), int index);

	PUBLIC void* (*get_head_data)(_THIS(List));
	PUBLIC void* (*get_tail_data)(_THIS(List));
	PUBLIC void* (*get_element_data)(_THIS(List), int index);

	PUBLIC int (*get_size)(_THIS(List));
	PUBLIC int (*contains)(_THIS(List), void * data_to_find, int (*compare)(void *, void *));
	PUBLIC int (*remove_inside)(_THIS(List), void * data_to_remove, int (*compare)(void*, void*));

	PUBLIC void (*set_custom_free)(_THIS(List), void (*custom_free)(void *));
	PUBLIC void (*insert_inside)(_THIS(List), void * data, size_t data_size, int index);
	PUBLIC void (*insert_tail)(_THIS(List), void * data, size_t data_size);
	PUBLIC void (*insert_head)(_THIS(List), void * data, size_t data_size);
	PUBLIC void (*clear)(_THIS(List));
	PUBLIC void (*clear_custom)(_THIS(List), void (*custom_free)(void *));
	PUBLIC void (*remove_head)(_THIS(List));
	PUBLIC void (*remove_tail)(_THIS(List));

	DESTRUCTOR(List);
};

FRIENDLY(head, Element)
FRIENDLY(tail, Element)
FRIENDLY(size, Element)

List *_init_List();

#endif //LO41_LIST_H
