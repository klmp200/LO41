#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "Objects.h"
#include "List/List.h"
#include "Building/Building.h"
#include "SharedData/SharedData.h"
#include "Visitor/Visitor.h"
#include "Resident/Resident.h"

void clean_exit(int error_code){
	printf("Signal %d received, exiting app\n", error_code);
	DELETE(GET_INSTANCE(SharedData));
	exit(0);
}

int main(int argc, char* argv[]) {
	SharedData * shared_data = GET_INSTANCE(SharedData);

	srand((unsigned int) time(NULL));
	signal(SIGINT, clean_exit);
	if(argc == 3){
		shared_data->set_main_building(shared_data, NEW(Building, argv[1], argv[2]));
	} else if (argc == 1){
		shared_data->set_main_building(shared_data, NEW(Building, "../residents.txt", "../visitors.txt"));
	} else{
		CRASH("Arguments invalides\nUsage : ./LO41 [residents_file visitors_file]\n");
	}

	shared_data->start_all_threads(shared_data);
	shared_data->wait_all_threads(shared_data);
	DELETE(shared_data);

	return 0;
}
