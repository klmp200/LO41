//
// Created by Antoine Bartuccio on 22/05/2018.
//

#ifndef LO41_OBJECTS_H
#define LO41_OBJECTS_H

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>

void agent_exit(int error_code);

#define NEW(type, ...) _init_##type(__VA_ARGS__)
#define GET_INSTANCE(type, ...) _get_instance_##type(__VA_ARGS__)
#define DELETE(obj) obj->_free_(obj)
#define DESTRUCTOR(type) void (*_free_)(struct o_##type *this)
#define THIS(type) type *this
#define _THIS(type) struct o_##type *this
#define CRASH(message) perror(message); raise(SIGINT); pthread_exit(NULL)

#define AGENT_OPTIONS sigset_t set; \
					  sigemptyset(&set); \
					  sigaddset(&set, SIGINT); \
					  signal(SIGUSR1, agent_exit); \
					  pthread_sigmask(SIG_BLOCK, &set, NULL); \
					  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL); \
					  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

#define SYNCHRONIZE
#define PRIVATE
#define PUBLIC
#define PROTECTED

#define FRIENDLY(property, obj)

#define LINK(type, obj, method) obj->method = method##_##type;
#define GETTER(obj_type, variable_type, variable) variable_type get_##variable##_##obj_type(obj_type *this){\
													return this->variable; }
#define SETTER(obj_type, variable_type, variable) void set_##variable##_##obj_type(obj_type *this, variable_type var){\
													this->variable = var; }

#define SYNCHRONIZED_GETTER(obj_type, variable_type, variable) variable_type get_##variable##_##obj_type(obj_type *this){\
														variable_type tmp_variable; \
														pthread_mutex_lock(&this->mutex_##variable); \
														tmp_variable = this->variable; \
														pthread_mutex_unlock(&this->mutex_##variable); \
														return tmp_variable; }
#define SYNCHRONIZED_SETTER(obj_type, variable_type, variable) void set_##variable##_##obj_type(obj_type *this, variable_type var){\
														pthread_mutex_lock(&this->mutex_##variable); \
														this->variable = var; \
														pthread_mutex_unlock(&this->mutex_##variable);}

void * malloc_or_die(size_t size);

/* Link all method from variadic list, support max 20 args */
#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define VA_NUM_ARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...) N
#define LINK_NB_(N) LINK_ALL_##N
#define LINK_NB(N) LINK_NB_(N)
#define LINK_ALL(type, obj, ...) LINK(type, obj, _free_) LINK_NB(VA_NUM_ARGS(__VA_ARGS__))(type, obj, __VA_ARGS__)
#define LINK_ALL_1(type, obj, method, ...) LINK(type, obj, method)
#define LINK_ALL_2(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_1(type, obj, __VA_ARGS__)
#define LINK_ALL_3(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_2(type, obj, __VA_ARGS__)
#define LINK_ALL_4(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_3(type, obj, __VA_ARGS__)
#define LINK_ALL_5(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_4(type, obj, __VA_ARGS__)
#define LINK_ALL_6(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_5(type, obj, __VA_ARGS__)
#define LINK_ALL_7(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_6(type, obj, __VA_ARGS__)
#define LINK_ALL_8(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_7(type, obj, __VA_ARGS__)
#define LINK_ALL_9(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_8(type, obj, __VA_ARGS__)
#define LINK_ALL_10(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_9(type, obj, __VA_ARGS__)
#define LINK_ALL_11(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_10(type, obj, __VA_ARGS__)
#define LINK_ALL_12(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_11(type, obj, __VA_ARGS__)
#define LINK_ALL_13(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_12(type, obj, __VA_ARGS__)
#define LINK_ALL_14(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_13(type, obj, __VA_ARGS__)
#define LINK_ALL_15(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_14(type, obj, __VA_ARGS__)
#define LINK_ALL_16(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_15(type, obj, __VA_ARGS__)
#define LINK_ALL_17(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_16(type, obj, __VA_ARGS__)
#define LINK_ALL_18(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_17(type, obj, __VA_ARGS__)
#define LINK_ALL_19(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_18(type, obj, __VA_ARGS__)
#define LINK_ALL_20(type, obj, method, ...) LINK(type, obj, method) LINK_ALL_19(type, obj, method, __VA_ARGS__)


#endif //LO41_OBJECTS_H
